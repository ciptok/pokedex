using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PokeDex.Tests
{
    public class FakeHttpMessageHandler<T> : HttpMessageHandler
    {
        private readonly T responseData;
        private readonly HttpStatusCode statusCode;

        public FakeHttpMessageHandler(T responseData, HttpStatusCode statusCode)
        {
            this.responseData = responseData;
            this.statusCode = statusCode;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(new HttpResponseMessage(statusCode)
            {
                Content = new StringContent(JsonConvert.SerializeObject(responseData))
            });
        }
    }
}