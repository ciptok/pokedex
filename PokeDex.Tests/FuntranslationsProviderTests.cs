﻿using PokeDex.Providers.Funtranslations;
using PokeDex.Providers.Funtranslations.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace PokeDex.Tests
{
    public class FuntranslationsProviderTests
    {
        [Fact]
        public async Task Funtranslations_Translate_Success()
        {
            var translation = new FuntranslationsResponse()
            {
                Success = new Success()
                {
                    Total = 1
                },
                Contents = new Contents()
                {
                    Text = "original",
                    Translated = "Yoda translation",
                    Translation = "yoda"
                }
            };

            var httpClient = new HttpClient(
                new FakeHttpMessageHandler<FuntranslationsResponse>(translation, HttpStatusCode.OK)) 
                { BaseAddress = new Uri("http://dummy")};

            var provider = new FuntranslationsProvider(httpClient);

            var response = await provider.TranslateAsync("yoda", "original");

            Assert.Equal("Yoda translation", response.Contents.Translated);
        }

        [Fact]
        public async Task Funtranslations_Translate_Fail()
        {
            var translation = new FuntranslationsResponse();

            var httpClient = new HttpClient(
                new FakeHttpMessageHandler<FuntranslationsResponse>(translation, HttpStatusCode.TooManyRequests)) 
                { BaseAddress = new Uri("http://dummy")};

            var provider = new FuntranslationsProvider(httpClient);

            var response = await provider.TranslateAsync("yoda", "original");

            Assert.True(null == response);
        }
    }
}
