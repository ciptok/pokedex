using NSubstitute;
using NSubstitute.ExceptionExtensions;
using PokeDex.Handler;
using PokeDex.Providers.Funtranslations;
using PokeDex.Providers.Funtranslations.Model;
using PokeDex.Providers.Pokeapi;
using PokeDex.Providers.Pokeapi.Model;
using Polly.CircuitBreaker;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace PokeDex.Tests
{
    public class PokemonHandlerTests
    {
        [Fact]
        public async Task Test_GetPokemon_Success()
        {
            var pokeapiProvider = CreatePokeapiProviderMock();
            var funtranslationProvider = Substitute.For<IFuntranslationsProvider>();

            var pokemonHandler = new PokemonHandler(pokeapiProvider, funtranslationProvider);
            var pokemon = await pokemonHandler.GetAsync("ditto");

            Assert.Equal("ditto", pokemon.Name);
            Assert.Equal("English description 1234", pokemon.Description);
            Assert.Equal("urban", pokemon.Habitat);
            Assert.True(false == pokemon.IsLegendary);
        }

        [Fact]
        public async Task Test_GetPokemon_NotFound()
        {
            var pokeapiProvider = CreatePokeapiProviderMock();
            var funtranslationProvider = Substitute.For<IFuntranslationsProvider>();

            var pokemonHandler = new PokemonHandler(pokeapiProvider, funtranslationProvider);
            var pokemon = await pokemonHandler.GetAsync("pok");

            Assert.True(null == pokemon);
        }

        [Theory]
        [InlineData("mewtwo", "Yoda translation", "cave", true)]
        [InlineData("ditto", "Shakespeare translation", "urban", false)]
        public async Task Test_GetPokemon_Translate_Success(string name, string expectedTranslation, 
            string expectedHabitat, bool expectedIsLegendary)
        {
            var pokeapiProvider = CreatePokeapiProviderMock();

            var funtranslationProvider = CreateFuntranslationProviderMock();

            var pokemonHandler = new PokemonHandler(pokeapiProvider, funtranslationProvider);
            var pokemon = await pokemonHandler.GetTranslatedAsync(name);

            Assert.Equal(name, pokemon.Name);
            Assert.Equal(expectedTranslation, pokemon.Description);
            Assert.Equal(expectedHabitat, pokemon.Habitat);
            Assert.True(expectedIsLegendary == pokemon.IsLegendary);
        }

        [Fact]
        public async Task Test_GetPokemon_Translation_Fails()
        {
            var pokeapiProvider = CreatePokeapiProviderMock();

            var funtranslationProvider = Substitute.For<IFuntranslationsProvider>();

            FuntranslationsResponse nullResponse = null;
            funtranslationProvider.TranslateAsync(Arg.Any<string>(), Arg.Any<string>())
                .Returns(nullResponse);

            var pokemonHandler = new PokemonHandler(pokeapiProvider, funtranslationProvider);
            var pokemon = await pokemonHandler.GetTranslatedAsync("ditto");

            Assert.Equal("ditto", pokemon.Name);
            Assert.Equal("English description 1234", pokemon.Description);
            Assert.Equal("urban", pokemon.Habitat);
            Assert.True(false == pokemon.IsLegendary);
        }

        [Fact]
        public async Task Test_GetPokemon_Translation_TooManyRequest_Fails()
        {
            var pokeapiProvider = CreatePokeapiProviderMock();

            var translation = new FuntranslationsResponse();
            var httpClient = new HttpClient(
                new FakeHttpMessageHandler<FuntranslationsResponse>(translation, HttpStatusCode.TooManyRequests))
            { BaseAddress = new Uri("http://dummy") };

            var funtranslationProvider = new FuntranslationsProvider(httpClient);

            var pokemonHandler = new PokemonHandler(pokeapiProvider, funtranslationProvider);
            var pokemon = await pokemonHandler.GetTranslatedAsync("ditto");

            Assert.Equal("ditto", pokemon.Name);
            Assert.Equal("English description 1234", pokemon.Description);
            Assert.Equal("urban", pokemon.Habitat);
            Assert.True(false == pokemon.IsLegendary);
        }

        [Fact]
        public async Task Test_GetPokemon_Translation_Communication_Fails()
        {
            var pokeapiProvider = CreatePokeapiProviderMock();

            var funtranslationProvider = Substitute.For<IFuntranslationsProvider>();

            funtranslationProvider.TranslateAsync(Arg.Any<string>(), Arg.Any<string>()).Throws(new BrokenCircuitException());

            var pokemonHandler = new PokemonHandler(pokeapiProvider, funtranslationProvider);
            var pokemon = await pokemonHandler.GetTranslatedAsync("ditto");

            Assert.Equal("ditto", pokemon.Name);
            Assert.Equal("English description 1234", pokemon.Description);
            Assert.Equal("urban", pokemon.Habitat);
            Assert.True(false == pokemon.IsLegendary);
        }


        private IPokeapiProvider CreatePokeapiProviderMock()
        {
            var pokeapiProvider = Substitute.For<IPokeapiProvider>();

            // ditto pokemon
            pokeapiProvider.GetSpeciesAsync(Arg.Is<int>(132)).Returns(new PokeapiPokemonSpecies()
            {
                Name = "ditto",
                Habitat = new KeyValue()
                {
                    Name = "urban",
                    Url = new Uri("https://pokeapi.co/api/v2/pokemon-habitat/8/")
                },
                IsLegendary = false,
                FlavorTextEntries = new FlavorTextEntry[]
                {
                    new FlavorTextEntry()
                    {
                        FlavorText = "English description 1234",
                        Language = new KeyValue()
                        {
                            Name = "en",
                            Url = new Uri("https://pokeapi.co/api/v2/language/9/")
                        },
                        Version = new KeyValue()
                        {
                            Name = "y",
                            Url = new Uri("https://pokeapi.co/api/v2/version/24/")
                        }
                    },
                    new FlavorTextEntry()
                    {
                        FlavorText = "English description 3333",
                        Language = new KeyValue()
                        {
                            Name = "en",
                            Url = new Uri("https://pokeapi.co/api/v2/language/9/")
                        },
                        Version = new KeyValue()
                        {
                            Name = "red",
                            Url = new Uri("https://pokeapi.co/api/v2/version/1/")
                        }
                    },
                    new FlavorTextEntry()
                    {
                        FlavorText = "Spanish description 2222",
                        Language = new KeyValue()
                        {
                            Name = "es",
                            Url = new Uri("https://pokeapi.co/api/v2/language/7/")
                        },
                        Version = new KeyValue()
                        {
                            Name = "y",
                            Url = new Uri("https://pokeapi.co/api/v2/version/24/")
                        }
                    },
                }
            });

            pokeapiProvider.GetAsync(Arg.Is<string>("ditto")).Returns(new PokeapiPokemon()
            {
                Name = "ditto",
                Species = new Species()
                {
                    Name = "ditto",
                    Url = new Uri("https://pokeapi.co/api/v2/pokemon-species/132/")
                }
            });


            //mewtwo pokemon
            pokeapiProvider.GetSpeciesAsync(Arg.Is<int>(150)).Returns(new PokeapiPokemonSpecies()
            {
                Name = "mewtwo",
                Habitat = new KeyValue()
                {
                    Name = "cave",
                    Url = new Uri("https://pokeapi.co/api/v2/pokemon-habitat/2/")
                },
                IsLegendary = true,
                FlavorTextEntries = new FlavorTextEntry[]
                {
                    new FlavorTextEntry()
                    {
                        FlavorText = "English description 6666",
                        Language = new KeyValue()
                        {
                            Name = "en",
                            Url = new Uri("https://pokeapi.co/api/v2/language/9/")
                        },
                        Version = new KeyValue()
                        {
                            Name = "y",
                            Url = new Uri("https://pokeapi.co/api/v2/version/1/")
                        }
                    },
                    new FlavorTextEntry()
                    {
                        FlavorText = "English description 7777",
                        Language = new KeyValue()
                        {
                            Name = "en",
                            Url = new Uri("https://pokeapi.co/api/v2/language/9/")
                        },
                        Version = new KeyValue()
                        {
                            Name = "red",
                            Url = new Uri("https://pokeapi.co/api/v2/version/2/")
                        }
                    },
                    new FlavorTextEntry()
                    {
                        FlavorText = "Spanish description 8888",
                        Language = new KeyValue()
                        {
                            Name = "es",
                            Url = new Uri("https://pokeapi.co/api/v2/language/7/")
                        },
                        Version = new KeyValue()
                        {
                            Name = "y",
                            Url = new Uri("https://pokeapi.co/api/v2/version/3/")
                        }
                    },
                }
            });

            pokeapiProvider.GetAsync(Arg.Is<string>("mewtwo")).Returns(new PokeapiPokemon()
            {
                Name = "mewtwo",
                Species = new Species()
                {
                    Name = "mewtwo",
                    Url = new Uri("https://pokeapi.co/api/v2/pokemon-species/150/")
                }
            });

            return pokeapiProvider;
        }

        private IFuntranslationsProvider CreateFuntranslationProviderMock()
        {
            var funtranslationProvider = Substitute.For<IFuntranslationsProvider>();
            
            funtranslationProvider.TranslateAsync(Arg.Is<string>("yoda"), Arg.Any<string>())
                .Returns(new FuntranslationsResponse()
                {
                    Success = new Success()
                    {
                        Total = 1
                    },
                    Contents = new Contents()
                    {
                        Text = "original",
                        Translated = "Yoda translation",
                        Translation = "yoda"
                    }
                });

            funtranslationProvider.TranslateAsync(Arg.Is<string>("shakespeare"), Arg.Any<string>())
                .Returns(new FuntranslationsResponse()
                {
                    Success = new Success()
                    {
                        Total = 1
                    },
                    Contents = new Contents()
                    {
                        Text = "original",
                        Translated = "Shakespeare translation",
                        Translation = "shakespeare"
                    }
                });

            return funtranslationProvider;
        }
    }
}
