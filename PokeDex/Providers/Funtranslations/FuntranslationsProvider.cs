﻿using Newtonsoft.Json;
using PokeDex.Providers.Funtranslations.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PokeDex.Providers.Funtranslations
{
    /// <summary>
    /// Funtranslation API provider
    /// </summary>
    public class FuntranslationsProvider : IFuntranslationsProvider
    {
        private readonly HttpClient _httpClient;

        private const string TranslateResource = "/translate";

        public FuntranslationsProvider(HttpClient httpClient)
        {
            this._httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<FuntranslationsResponse> TranslateAsync(string translator, string text)
        {
            var request = new FuntranslationsRequest()
            {
                Text = text
            };

            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync($"{TranslateResource}/{translator}.json", content);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }

            return JsonConvert.DeserializeObject<FuntranslationsResponse>(await response.Content.ReadAsStringAsync());
        }
    }
}
