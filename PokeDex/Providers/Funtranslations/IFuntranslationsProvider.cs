﻿using PokeDex.Providers.Funtranslations.Model;
using System.Threading.Tasks;

namespace PokeDex.Providers.Funtranslations
{
    public interface IFuntranslationsProvider
    {
        /// <summary>
        /// Translate text with given translator
        /// </summary>
        /// <param name="translator"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        Task<FuntranslationsResponse> TranslateAsync(string translator, string text);
    }
}
