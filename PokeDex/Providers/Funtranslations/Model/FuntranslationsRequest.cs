﻿using Newtonsoft.Json;

namespace PokeDex.Providers.Funtranslations.Model
{
    public class FuntranslationsRequest
    { 
        [JsonProperty("text")]
        public string Text { get; set; }
    }
}
