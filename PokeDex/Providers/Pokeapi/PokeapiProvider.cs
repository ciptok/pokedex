﻿using Newtonsoft.Json;
using PokeDex.Providers.Pokeapi.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace PokeDex.Providers.Pokeapi
{
    public class PokeapiProvider : IPokeapiProvider
    {
        private readonly HttpClient _httpClient;

        private const string PokemonResource = "/api/v2/pokemon";
        private const string PokemonSpeciesResource = "/api/v2/pokemon-species";


        public PokeapiProvider(HttpClient httpClient)
        {
            this._httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
        }

        public async Task<PokeapiPokemon> GetAsync(string name)
        {
            var response = await _httpClient.GetAsync($"{PokemonResource}/{name}");

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }

            var data = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<PokeapiPokemon>(data);
        }

        public async Task<PokeapiPokemonSpecies> GetSpeciesAsync(int id)
        {
            var response = await _httpClient.GetAsync($"{PokemonSpeciesResource}/{id}");

            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }

            return JsonConvert.DeserializeObject<PokeapiPokemonSpecies>(await response.Content.ReadAsStringAsync());
        }
    }
}
