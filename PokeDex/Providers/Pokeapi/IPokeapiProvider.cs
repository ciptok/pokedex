﻿using PokeDex.Providers.Pokeapi.Model;
using System.Threading.Tasks;

namespace PokeDex.Providers.Pokeapi
{
    public interface IPokeapiProvider
    {
        /// <summary>
        /// Get Pokeman details
        /// </summary>
        /// <param name="name">Pokemon name</param>
        /// <returns>Pokemon data model</returns>
        Task<PokeapiPokemon> GetAsync(string name);

        /// <summary>
        /// Get Pokemon species details
        /// </summary>
        /// <param name="id">Species identifier</param>
        /// <returns>Pokemon species data model</returns>
        Task<PokeapiPokemonSpecies> GetSpeciesAsync(int id);
    }
}
