﻿using PokeDex.Model;
using System.Threading.Tasks;

namespace PokeDex.Handler
{
    public interface IPokemonHandler
    {
        /// <summary>
        /// Get pokemon by name
        /// </summary>
        /// <param name="name">Pokemon name</param>
        /// <returns></returns>
        Task<Pokemon> GetAsync(string name);

        /// <summary>
        /// Get pokemon by name and translate description 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<Pokemon> GetTranslatedAsync(string name);
    }
}
