﻿using PokeDex.Model;
using PokeDex.Providers.Funtranslations;
using PokeDex.Providers.Pokeapi;
using Polly.CircuitBreaker;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PokeDex.Handler
{
    /// <summary>
    /// Pokemon handler to consume and process external APIs
    /// </summary>
    public class PokemonHandler : IPokemonHandler
    {
        private readonly IPokeapiProvider _pokeapiProvider;
        private readonly IFuntranslationsProvider _funtranslationsProvider;

        public PokemonHandler(IPokeapiProvider pokeapiProvider, IFuntranslationsProvider funtranslationsProvider)
        {
            this._pokeapiProvider = pokeapiProvider ?? throw new ArgumentNullException(nameof(pokeapiProvider));
            this._funtranslationsProvider = funtranslationsProvider ?? throw new ArgumentNullException(nameof(funtranslationsProvider));
        }

        public async Task<Pokemon> GetAsync(string name)
        {
            var pokemon = await _pokeapiProvider.GetAsync(name);

            if (pokemon == null)
            {
                return null;
            }

            var segments = pokemon.Species.Url.Segments;
            var speciesId = segments[segments.Length - 1].TrimEnd('/');

            var species = await _pokeapiProvider.GetSpeciesAsync(Convert.ToInt32(speciesId));

            if (species == null)
            {
                return null;
            }

            var response = new Pokemon()
            {
                Name = pokemon.Name,
                Description = species.FlavorTextEntries.Where(x => x.Language.Name == "en")
                                                       .Select(s => s.FlavorText)
                                                       .First(),
                Habitat = species.Habitat.Name,
                IsLegendary = species.IsLegendary
            };

            return response;
        }

        public async Task<Pokemon> GetTranslatedAsync(string name)
        {
            var pokemon = await GetAsync(name);

            string language;
            if (pokemon.Habitat == "cave")
            {
                language = "yoda";
            }
            else
            {
                language = "shakespeare";
            }

            try
            {
                var translation = await _funtranslationsProvider.TranslateAsync(language, pokemon.Description);

                if (translation != null && translation.Success.Total > 0)
                {
                    pokemon.Description = translation.Contents.Translated;
                }
            }
            catch (BrokenCircuitException)
            {
                // If can't establish connection we use original description
            }

            return pokemon;
        }
    }
}
