﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PokeDex.Handler;
using PokeDex.Model;
using Polly.CircuitBreaker;
using System;
using System.Net;
using System.Threading.Tasks;

namespace PokeDex.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PokemonController : ControllerBase
    {
        private readonly IPokemonHandler _pokemonHandler;
        private readonly ILogger<PokemonController> _logger;

        public PokemonController(IPokemonHandler pokemonHandler, ILogger<PokemonController> logger)
        {
            this._pokemonHandler = pokemonHandler ?? throw new System.ArgumentNullException(nameof(pokemonHandler));
            this._logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// Get pokemon details by name
        /// </summary>
        /// <remarks>
        /// GET api/Pokemon/{name}
        /// </remarks>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("{name}")]
        [ProducesResponseType(typeof(Pokemon),StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status502BadGateway)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Pokemon>> GetAsync(string name)
        {
            try
            {
                var pokemon = await _pokemonHandler.GetAsync(name);

                if (pokemon == null)
                {
                    return NotFound();
                }

                return pokemon;
            }
            catch (BrokenCircuitException ex)
            {
                _logger.LogError($"Connection error: {ex.ToString()}");

                return StatusCode((int)HttpStatusCode.BadGateway, "Service currently unavailable");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Unexpected error: {ex.ToString()}");

                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Get pokemon details by name and translate description
        /// </summary>
        /// <remarks>
        /// GET api/Pokemon/translate/{name}
        /// </remarks>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("translated/{name}")]
        [ProducesResponseType(typeof(Pokemon), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status502BadGateway)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Pokemon>> GetTranslatedAsync(string name)
        {
            try
            {
                var pokemon = await _pokemonHandler.GetTranslatedAsync(name);

                if (pokemon == null)
                {
                    return NotFound();
                }

                return pokemon;
            }
            catch (BrokenCircuitException ex)
            {
                _logger.LogError($"Connection error: {ex.ToString()}");

                return StatusCode((int)HttpStatusCode.BadGateway, "Service currently unavailable");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Unexpected error: {ex.ToString()}");

                return StatusCode((int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
