﻿## PokeDex description

**Prerequisite:**
- .NET 5
- Docker-desktop and WSL installed to run in container on Windows

**Notes**
- Each API provider is decoupled and have only simple validations
- Pokemon handler interface is meant to implement different strategies for processing
- Existing pokemon handler considers Funstranslations API as optional functionality

**Running and testing:**
- Run projects from Visual Studio:
```
PokeDex
PokeDex.Tests (from Test Explorer)
```

- Build and run from terminal:
```
run build_and_run_all_services.cmd
run run_tests.cmd
```	

- Docker compose
```
docker-compose up
```

- Query API through browser:
	http://localhost:5000/swagger

**TODO:**
- [ ] Improve error handling and add more logging
- [ ] Add API key support for Funstranslation
- [ ] Add helm charts for k8s deployment